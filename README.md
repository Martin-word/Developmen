### 登陆用户名密码 .env

### NPS 密码

### ./nps/nps.conf -> web_username or web_password

### 快速构建容器并且显示 jenkins 的密码

```
yum install -y libltdl.so.7
docker-compose up -d
docker-compose exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

### 快速删除容器

```
docker-compose kill
docker-compose rm
y
```

### 删除目录

```
rm -rf consul influxdb jenkins mariadb mongo postgres rabbitmq redis
```

